package org.mik.zoo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.mik.zoo.factory.FileBasedFactory;
import org.mik.zoo.factory.LivingBeingFactory;
import org.mik.zoo.livingbeing.LivingBeing;
import org.mik.zoo.livingbeing.animal.Animal;
import org.mik.zoo.livingbeing.animal.AnimalType;

/**
 * Main class
 *
 * @author
 *
 */
public class App {

	/**
	 * 
	 */
	private static final String FILE_NAME = "data.txt"; //$NON-NLS-1$

	private List<LivingBeing> livingBeings;
	private Map<AnimalType, Animal> animalsByType;

	/**
	 * 
	 */
	public App() {
		this.livingBeings = new ArrayList<>();
		this.animalsByType = new HashMap<>();
	}

	/**
	 * Entry point of the application
	 *
	 * @param args
	 *            arguments
	 */
	public static void main(String[] args) {
		new App().start();
	}

	/**
	 * Invoke methods
	 */
	private void start() {
		this.loadData();
		this.organizeAnimals();
		this.printAnswers();
		this.printAnimalsByType();
	}

	/**
	 * Load data
	 */
	private void loadData() {
		try {
			LivingBeingFactory factory = new FileBasedFactory(FILE_NAME);

			this.livingBeings.addAll(factory.loadData());
		} catch (Exception e) {
			System.err.println("Error during data loading: " + e.getMessage()); //$NON-NLS-1$
			e.printStackTrace();
		}
	}

	/**
	 * Answer the questions of the kids visiting the zoo
	 */
	private void printAnswers() {
		System.out.println("\nQuestions - answers\n"); //$NON-NLS-1$
		System.out.println("How many animals are there here?"); //$NON-NLS-1$

		System.out.println("How many elephants are there here?"); //$NON-NLS-1$

		System.out.println("Are there predators here?"); //$NON-NLS-1$

		System.out.println("Is Tux here?"); //$NON-NLS-1$
	}

	/**
	 * Count animals
	 *
	 * @return number of animals
	 */
	private int countAnimals() {
		return 0;
	}

	/**
	 * Count elephants
	 *
	 * @return number of elephants
	 */
	// TODO homework
	private int countElephants() {
		return 0;
	}

	/**
	 * Find a living being by the given name
	 *
	 * @param name
	 *            the name
	 * @return LivingBeing instance if found or null, if the name was null or no
	 *         LivingBeing for this name
	 */
	private LivingBeing findByName(String name) {
		return null;
	}

	/**
	 * Collect animals by their types
	 */
	private void organizeAnimals() {
		for (LivingBeing livingBeing : this.livingBeings) {
			if (livingBeing instanceof Animal) {
				Animal animal = (Animal) livingBeing;

				this.animalsByType.put(animal.getAnimalType(), animal);
			}
		}
	}

	/**
	 * Printing animals by their types
	 */
	private void printAnimalsByType() {
		System.out.println("\nAnimals by type\n"); //$NON-NLS-1$
	}
}
