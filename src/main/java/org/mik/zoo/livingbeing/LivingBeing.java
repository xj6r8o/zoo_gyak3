package org.mik.zoo.livingbeing;

public interface LivingBeing {

	public String getScientificName();

	public String getInstanceName();

	public String getImageURL();
}
